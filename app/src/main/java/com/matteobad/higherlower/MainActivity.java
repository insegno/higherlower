package com.matteobad.higherlower;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;


public class MainActivity extends AppCompatActivity {

    int randomNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Random random = new Random();
        randomNumber = random.nextInt((20 - 1) + 1) + 1;
        Log.i("randomNumber", String.valueOf(randomNumber));
    }


    public void prova(View view) {

        EditText numberEditText = (EditText) findViewById(R.id.numberEditText);
        String stringInserita = numberEditText.getText().toString();


        try {
            int numeroInserito = Integer.valueOf(stringInserita);
            int risultato = comparaNumeri(numeroInserito);

            String messaggioPerUtente;
            switch (risultato) {
                case -1:
                    messaggioPerUtente = "Lower";
                    break;
                case 0:
                    messaggioPerUtente = "Correct";
                    break;
                case 1:
                    messaggioPerUtente = "Higher";
                    break;
                default:
                    messaggioPerUtente = "Errore imprevisto";
            }

            Toast.makeText(MainActivity.this, messaggioPerUtente, Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "Devi inserire un numero!", Toast.LENGTH_SHORT).show();
        }

    }


    private int comparaNumeri(int numeroInserito) {
        if (numeroInserito < randomNumber) return -1;
        else if (numeroInserito == randomNumber) return 0;
        else if (numeroInserito > randomNumber) return 1;
        else return 2;
    }

}

